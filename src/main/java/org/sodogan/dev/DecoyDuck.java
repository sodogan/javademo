/**
 *
 */
package org.sodogan.dev;

/**
 * @author User
 *
 */
public class DecoyDuck extends Duck {

    public DecoyDuck() {
        super("Corpus Decoyus");
    }


    @Override
    public void fly() {
        System.out.println(get_latinName() + " is flying ");
    }

    @Override
    public void quack() {
        System.out.println(get_latinName() + " is quacking  ");
    }

    @Override
    public void display() {
        System.out.println("Inside Display Duck:  " + get_latinName());
    }

}
