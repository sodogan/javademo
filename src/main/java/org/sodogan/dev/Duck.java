package org.sodogan.dev;

/**
 * @author Solen Dogan
 *
 */
public abstract class Duck {


    protected String _latinName;

    public Duck(String _latinName) {
        this._latinName = _latinName;
    }

    public String get_latinName() {
        return _latinName;
    }


    public abstract void fly();

    public abstract void  quack() ;

    public abstract void display();


}
