package org.sodogan.dev;

public class RedHeadedDuck extends Duck {

    public RedHeadedDuck() {
        super("Corpus Decoyus");
    }


    @Override
    public void fly() {
        System.out.println(get_latinName() + " is flying ");
    }

    @Override
    public void quack() {
        System.out.println(get_latinName() + " is quacking  ");
    }

    @Override
    public void display() {
        System.out.println("Inside Display Duck:  " + get_latinName());
    }

}
